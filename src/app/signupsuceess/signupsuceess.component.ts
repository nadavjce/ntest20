import { LoginComponent } from './../login/login.component';
import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signupsuceess',
  templateUrl: './signupsuceess.component.html',
  styleUrls: ['./signupsuceess.component.css']
})
export class SignupsuceessComponent implements OnInit {


  constructor(public authService: AuthService) { }

  ngOnInit() {
    console.log(this.authService.user)
}

}