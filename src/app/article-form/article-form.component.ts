import { AuthService } from './../auth.service';
//import { UsersService } from './../users.service';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Posts } from '../interfaces/posts';
import { Users } from '../interfaces/users';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {
  postsdata$: Posts[]=[];
  usersdata$: Users[]=[];
  author:string;
  authorMail:string;
  message:string;
  userID;
  name;
  Id;
  title;
  body;
  postID;
  postsData$;
  usersName$;
  posts$:Observable<any>;
  userId:string;
  public count : number=0;

  constructor(private route:ActivatedRoute,private router: Router,
              private postsService:PostsService,
              private authService:AuthService,)
  {
  }

  ngOnInit()
  {
   // return this.postsService.getArticles(this.userID)
    //this.postsData$ = this.postsService.searchPostsData();
    this.authService.user.subscribe(
      user => {
        this.userID = user.uid;
        this.posts$ = this.postsService.getPost();
      })
      
  // onclick()
  // {
  //   this.count+=1;
  // }
}
}