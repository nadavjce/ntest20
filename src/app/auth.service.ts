import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService 
{
  user:Observable<User | null>

  constructor(public afAuth:AngularFireAuth, 
              public router: Router ,private route:ActivatedRoute) 
              {
                this.user = this.afAuth.authState;
              }
  
  
  


  signUp(email:string, password:string)
  {
    console.log('In SignUp()')
    console.log("this is the email: ",email)
    console.log("this is the password: ",password);
    this.afAuth.auth.createUserWithEmailAndPassword(email,password).then(
                                                                          user =>
                                                                          {
                                                                            console.log('Succesful sign up')
                                                                            this.router.navigate(['/signupsuceess']);
                                                                          }
                                                                        )
                                                                        .catch(function(error) 
                                                                                        {
                                                                                          // Handle Errors here.
                                                                                          var errorCode = error.code;
                                                                                          var errorMessage = error.message;
                                                                                          if (errorCode === 'auth/wrong-password') {
                                                                                            alert('Wrong password.');
                                                                                          }
                                                                                          else {
                                                                                            alert(errorMessage);
                                                                                          }
                                                                                          console.log(error);
                                                                                        });                                                                  

  }

  Login(email:string, password:string)
  {
    console.log('In Login() in auth.service')
    console.log("the user email: ",email)
    this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
                                                                      (res) => 
                                                                      {
                                                                      //  this.user=res
                                                                      //  console.log("res is: ",res.user)
                                                                      //  this.user.subscribe(
                                                                      //                       user=>
                                                                      //                       {
                                                                      //                         console.log('This is the userID logged in: ',user.uid)
                                                                      //                       }
                                                                      //                     );
                                                                        console.log("Login successfully")
                                                                        this.router.navigate(['/signupsuceess']);
                                                                      }
                                                                    )
                                                                    .catch(function(error) 
                                                                                        {
                                                                                          // Handle Errors here.
                                                                                          var errorCode = error.code;
                                                                                          var errorMessage = error.message;
                                                                                          if (errorCode === 'auth/wrong-password') {
                                                                                            alert('Wrong password.');
                                                                                          } else {
                                                                                            alert(errorMessage);
                                                                                          }
                                                                                          console.log(error);
                                                                                        });
                                                                                        
  }

  LogOut()
  {
    console.log("in auth.service - LogOut()")
    this.afAuth.auth.signOut();
    this.router.navigate(['/'])
  }

  getUser()
  {
    //console.log("user Observable: ",this.user)
    return this.user;
  }


  
}
