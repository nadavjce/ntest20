// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBWeq01urpmc_QeZauHXD_pcPMouCzYhcA",
    authDomain: "ntest20-f6f90.firebaseapp.com",
    databaseURL: "https://ntest20-f6f90.firebaseio.com",
    projectId: "ntest20-f6f90",
    storageBucket: "ntest20-f6f90.appspot.com",
    messagingSenderId: "1073652635524"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
