import { AuthService } from './../auth.service';
//import { UsersService } from './../users.service';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Posts } from '../interfaces/posts';
import { Users } from '../interfaces/users';
import { Comments } from '../interfaces/comments';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  postsdata$: Posts[]=[];
  usersdata$: Users[]=[];
  commentsdata$: Comments[] =[];
  author:string;
  authorMail:string;
  message:string;
  userID;
  name;
  Id;
  title;
  body;
  postID;
  postsData$;
  usersName$;


  constructor(private router: Router,
              private postsService:PostsService,
              private authService:AuthService,)
  {
  }

  ngOnInit()
  {
    this.postsData$ = this.postsService.searchPostsData();
    this.postsService.getComments().subscribe(data =>this.commentsdata$ = data);
   // this.postsData$ = this.postsService.getPost(this.user);
   this.authService.getUser().subscribe( 
    user=>
    {
      this.userID = user.uid
      console.log('userID: ', this.userID);
    }
  );

    console.log(this.postsData$.subscribe(data => console.log("data: "+ data)));
  }

  createPosts()
  {
    console.log("in createPosts")
    this.postsService.addPostsToCollection();
    //this.router.navigate(['/posts']);
    console.log("Post Added");
  }

  // deletePosts(postID:string)
  // {
  //   console.log("In - books.ts deleteBooks() ")
  //   console.log("this is the postID: ",postID)
  //   this.postsService.deletePostFromCollection(this.userID,this.postID);
  // }
  savePosts(title:string,body:string){
    this.postsService.savePost(this.userID,title,body)
    this.message = "saved for later viewing"
  }
  // savePosts(){
  //   for (let index = 0; index < this.postsdata$.length; index++) {
  //     for (let i = 0; i < this.usersdata$.length; i++) {
  //       if (this.postsdata$[index].userId=this.usersdata$[i].id) {
  //         this.postsService.savePost(this.userID,this.postID,this.postsdata$[index].title,this.usersdata$[i].name)
  //         console.log(this.postsdata$[index].title)
  //       } 
  //     }
  //   }
  //   this.message="Uploaded";
    
  // }
  
}
