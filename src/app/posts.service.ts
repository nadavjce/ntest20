import { PostsComponent } from './posts/posts.component';
//import { UsersComponent } from './users/users.component';
//import { Users } from './interfaces/users';
import { Posts } from './interfaces/posts';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Users } from './interfaces/users';
import { Comments } from './interfaces/comments';

@Injectable({
  providedIn: 'root'
})
export class PostsService{

  private URL="https://jsonplaceholder.typicode.com/posts";
  private URLComent="https://jsonplaceholder.typicode.com/comments/"

  //private URL2="https://jsonplaceholder.typicode.com/users";
  private posts: Posts[] = [];
  postsCollaction: AngularFirestoreCollection=this.db.collection('Posts');
  userCollection: AngularFirestoreCollection= this.db.collection('Users');
  commentCollection: AngularFirestoreCollection=this.db.collection('Comments');


  constructor(private http:HttpClient, private db:AngularFirestore) 
  {
    this.postsCollaction= db.collection<Posts>('Posts')
    this.userCollection=db.collection<Users>('Users')
    this.commentCollection=db.collection<Comments>('Comments')
    }



  searchPostsData():Observable<Posts[]>
  {
    // console.log(this.http.get<Post[]>(`${this.URL}`));
    return this.http.get<Posts[]>(`${this.URL}`)
    // .pipe(
    //   map(data =>  this.addUsersToPosts(data))
    //);
  }

// getAllUsers(): Observable<Users[]>{
//     const users = this.http.get<Users[]>(`${this.URL2}`);
//     return users;
// }
addNewPost(name:string,title:string,body:string,userID:string)
{
  console.log("in postService: addNewPost()")
  const post = {
                name:name,
                title:title,
                body:body,
                userID:userID
               }
               console.log("this is the post: ", post)
  this.userCollection.doc(userID).collection('Posts').add(post);
  // this.db.collection('Posts').add(post);
}

getComments():Observable<Comments[]>{
  return this.http.get<Comments[]>(`${this.URLComent}`);
  }

  getArticles(userID): Observable<any[]> 
  {
    // return this.db.collection('Books').valueChanges({idField:'key_id'});
    this.postsCollaction = this.db.collection(`Users/${userID}/posts`);
    //console.log("this.articleCollection: ",this.articleCollection)
    return this.postsCollaction.snapshotChanges().pipe(
                                                        map(
                                                          collection => collection.map(
                                                                                        document =>
                                                                                        {
                                                                                          const data= document.payload.doc.data();
                                                                                          data.id= document.payload.doc.id;
                                                                                          return data;
                                                                                        }
                                                                                      )
                                                          )
                                                      );
  } 
getPost(): Observable<any[]>
{
  //this.postsCollaction = this.db.collection(`Users/${user.ID}/Posts`);
  return this.http.get<Posts[]>(`${this.URL}`)      //get all posts from Posts Collection
  // return this.postsCollaction.snapshotChanges().pipe(
  //                                                     map(
  //                                                       collection => collection.map(
  //                                                                                     document =>
  //                                                                                     {
  //                                                                                       const data= document.payload.doc.data();
  //                                                                                       //data.id = document.payload.doc.id;
  //                                                                                       return data;
  //                                                                                     }
  //                                                                                   )
  //                                                       )
  //                                                   );
} 


// getOnePost(userID:string,postID:string):Observable<any>
//     {
//       console.log("this is the postID: ",postID);
//      // return this.db.doc(`Posts/${postID}`).get();
//     //  const post = {userID:userID, title:title, body:body}
//      this.userCollection.doc(userID).collection('posts').get();
      
//     }

  // deletePostFromCollection(userID,postID)
  // {
  //   console.log("In - Posts.service deletePostFromCollection() ")
  //   console.log("This is the postID: "+postID)
  //   console.log("This is the userID: "+userID)
  //   //this.db.doc(`Posts/${postID}`).delete();
  //   this.db.doc(`Users/${userID}/Posts/${postID}`).delete();
  //   console.log(postID+" have deleted")
  // }
    
    
    updatePost(postID,title:string,name:string, body:string)
    {
      this.db.doc(`Posts/${postID}`).update(
        {
          name:name,
          title:title,
          body:body
        })
    }

//   addUsersToPosts(data: Posts[]):Posts[]
//   {
    
//     const users = this.getAllUsers();
//     const postsArray =[];
//     users.forEach(user => 
//       {
//         user.forEach(u =>
//           {
//             data.forEach(post =>
//               {
//                 if(post.userId == u.id)
//                 {
//                     postsArray.push(
//                       {
//                         id: post.id,
//                         userId:post.userId,
//                         title: post.title,
//                         body: post.body,
//                         userName: u.name,
//                       })
//                 }
//             })
//         })
//     })

//     return postsArray;
// }
addPost(userID:string,postID:string,title:string,author:string,body:string){
  const post = {userId:userID,id:postID,title:title, author:author,body:body}
  //this.userCollection.doc(userID).collection('Users').add(post); 
  console.log('post')
  this.db.collection<Users>('Users').doc(userID).collection<Posts>('Posts').add(post);
}

savePost(userID:string , title:string, body:string, ){
  const post = {userID:userID, title:title, body:body}
  this.userCollection.doc(userID).collection('posts').add(post);
  console.log("post: " ,post)
}

addPostsToCollection()
     {
//       // console.log("posts service method")
//       //const users=this.getAllUsers();
//       const posts= this.http.get<Posts[]>(`${this.URL}`);
//       let collactionOfPosts:Observable<Posts[]>;
//       collactionOfPosts= this.postsCollaction.valueChanges();

//       collactionOfPosts.forEach(data=>{
//         console.log(data.length)
//         if(data.length===0)
//         {
//           posts.forEach(post =>
//             {
//             post.forEach(singalPost =>
//               {
//               users.forEach(user =>
//                 {
//                user.forEach(singalUser =>
//                 {
//                  if(singalUser.id===singalPost.userId)
//                  {
//                    singalPost.userName=singalUser.name;
//                    this.db.collection('Posts').add(
//                      {
//                      title: singalPost.title,
//                      name:singalUser.name,
//                      body:singalPost.body
//                    })
//                  }
//                })
//               })
//             })
//           })
//         }
//         else
//         {
//           console.log("This posts have added already")
//         }
//       })




  }


}
